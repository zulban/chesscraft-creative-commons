# ChessCraft Creative Commons

Here you can find all the art assets used in [ChessCraft](http://www.chesscraft.ca) shared under a creative commons license. You can [download the whole repository here](https://gitlab.com/zulban/chesscraft-creative-commons/-/archive/master/chesscraft-creative-commons-master.zip). If you find art or music in ChessCraft that isn't here, let me know and I'll add it.

Enjoy!

# Pieces

Browse around 100 chess-like pieces [in this folder](https://gitlab.com/zulban/chesscraft-creative-commons/tree/master/pieces).

![chesscraft pieces preview](previews/pieces.png)

# Adventure Map

Browse the [adventure map folder](https://gitlab.com/zulban/chesscraft-creative-commons/tree/master/adventure) to see adventure maps, and the many tiny images used to build them in the [doodad palette](https://gitlab.com/zulban/chesscraft-creative-commons/tree/master/adventure/doodad-palette).

![chesscraft adventure map preview](previews/overworld.png)

# License

All files in this repository are shared under the [Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.

# Attribution

### Music

The music in game is shortened to save space. The `music` folder here has the full songs. The artists:

* [One Man Symphony](https://onemansymphony.bandcamp.com/releases)
* `Politique 2.0`
* [Meydan](https://meydan.bandcamp.com/)
* Persia. This is the name of my cat. Twice she has jumped on our electronic piano, hit record, and played a tune. She is the honest to goodness composer and pianist of the "draw" and "loss" musical tunes.

In each case, I asked the artist if we can release their music under the same license. They all agreed, except my cat, who did not respond.

### Emoji

The emoji are from [openmoji](https://openmoji.org/) and share the same license.

### Wikimedia

Some pieces were copied from the Wikimedia Commons [SVG chess pieces](https://commons.wikimedia.org/wiki/Category:SVG_chess_pieces), sharing the same license:

* classic chess pieces
* commoner
* boat
* zebra
* giraffe
* archbishop
* chancellor
